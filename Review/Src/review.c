#include <stdio.h>
#include <math.h>
#include <stdlib.h>

/*//Hàm input
void input(int *in)
{
    printf("Number : \n");
    scanf("%d",in);
}
//Hàm input a b n
void input(int *a,int *b,int *n)
{
    printf("A? : \n");
    scanf("%d",a);
    printf("B? : \n");
    scanf("%d",b);
    printf("N? : \n");
    scanf("%d",n);
}*/

//Hàm kt số đối xứng
int ktsodoixung(int x)
{
    int temp=x;
    int X=x;
    int x_2=x;
    int dem=0;
    int new_x=0;
    while(temp!=0)
    {
        temp=temp/10;
        dem++;
    }
    dem--;
    while(x!=0)
    {
        x_2=x;
        x=x/10;
        x_2=x_2%10;
        new_x=new_x+x_2*pow(10,dem);
        dem--;
    }
    if(new_x==X)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

//Chữ số lớn nhất chữ số nhỏ nhất
void output(int *arr);
int maxnum(int num)
{
    int temp=0, numlone=num;
    int max=0,min=0;
    int arr[2];
    while(numlone!=0)
    {
        num=numlone;
        numlone=numlone/10;
        num=num%10;
        temp=num;
        min=temp;
        if(temp>=max)
        {
            max=temp;
        }
        if(temp<min)
        {
            min=temp;
        }
    }
    arr[0]=max;
    arr[1]=min;
    output(arr);
}
void output(int *arr)
{
    printf("Max : %d\n",arr[0]);
    printf("Min : %d",arr[1]);
}

//Hàm kt số chính phương
int ktsochinhphuong(int num)
{
    if(num==0)
    {
        return 0;
    }
    int temp=0;
    temp=sqrt(num);
    temp=pow(temp,2);
    if(num==temp)
    {
        return 1;
    }
    else
    {
        return 0;
    }
    
}

//Hàm kt số nguyên tố
int ktsonguyento(int num)
{
    if(num<=1)
    {
        return 0;
    }
    for(int i=num-1;i>1;i--)
    {
        if(num%i==0)
        {
            return 0;
        }
    }
    return 1;
}

//kt số tăng giảm
int kttang_giam(int num)
{
    int temp=num,dem=0;
    int dem2=1,max=0;
    while(temp!=0)
    {
        temp=temp/10;
        dem++;
    }
    int dem3=dem;
    int *arr,temp2=dem;
    arr=(int*)malloc(sizeof(int)*dem);
    dem--;
    while(num!=0)
    {
        temp=num;
        num=num/10;
        temp=temp%10;
        arr[dem]=temp;
        dem--;
    }
    max=arr[0];
    for(int i=1;i<temp2;i++)
    {
       if(arr[i]>=max)
       {
           max=arr[i];
           dem2++;
       } 
    }
    if(dem2==dem3)
    {
        return 1;  //chuỗi số tăng
    }
    else
    {
        return 0; //chuỗi giảm hoặc ko tăng ko giảm
    }
    
}

//Nhập 3 số nguyên a, b và n với a, b < n. Tính tổng các số nguyên dương 
//nhỏ hơn n chia hết cho a nhưng không chia hết cho b.
int sum_a_b_n(int a,int b,int n)
{
    int sum=0;
    if(a>n&&b>n)
    {
        return 0;
    }
    for(int i=1; i<n; i++)
    {
        if(i<n&&i%a==0&&i%b!=0)
        {
            sum=sum+i;
        }
    }
    return sum;
}

//Mảng kiểm tra số nguyên tố
int checkarr(int *arr,int n)
{
    for(int j=0;j<n;j++)
    {
        if(arr[j]<=1)
        {
            return 0;
        }
    }
    for(int x=0;x<n;x++)
    {
        for(int i=arr[x]-1;i>1;i--)
        {
            if(arr[x]%i==0)
            {
                return 0;
            }
        }
    }
    return 1;
}

